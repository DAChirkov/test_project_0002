terraform {
  backend "s3" {
    bucket = "terraform-8936534"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}
