terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = var.region_name

  default_tags {
    tags = {
      Name = var.tags_name
    }
  }
}

# Create Network Security Group
resource "aws_security_group" "nsg" {
  name_prefix = var.nsg_name

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create RDS (PostgreSQL)
resource "aws_db_instance" "db" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "postgres"
  engine_version         = "15.3"
  instance_class         = "db.t3.micro"
  identifier             = "mydb-001"
  username               = "dbuser"
  password               = "dbpass123"
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.nsg.id]
}

# Create Elastic Beanstalk
resource "aws_elastic_beanstalk_application" "app" {
  name = var.app_name
}

# Create environment Elastic Beanstalk
resource "aws_elastic_beanstalk_environment" "environment_django" {
  name = var.environment_name

  application         = aws_elastic_beanstalk_application.app.name
  cname_prefix        = var.domain_name
  solution_stack_name = "64bit Amazon Linux 2023 v4.0.3 running Python 3.9"

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "AWS_ServiceRoleForElasticBeanstalkEC2"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "DB_HOST"
    value     = aws_db_instance.db.endpoint
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "DB_PORT"
    value     = aws_db_instance.db.port
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "DB_USER"
    value     = aws_db_instance.db.username
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "DB_PASS"
    value     = aws_db_instance.db.password
  }
}

