variable "region_name" {
  default = "us-east-1"
}

variable "tags_name" {
  default = "test_project0002"
}

variable "nsg_name" {
  default = "NSG"
}

variable "app_name" {
  default = "ebdjango-app-0001"
}

variable "environment_name" {
  default = "ebdjango-app-env-0001"
}

variable "domain_name" {
  default = "testproject-0002"
}
