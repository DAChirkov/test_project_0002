from django.shortcuts import render
from django.http import HttpResponse
from .models import Item


def index(request):
    return HttpResponse("<center><h1>Hello World from Django Server!</h1></center>")

def item_list(request):
    items = Item.objects.all()
    return render(request, 'app_1/item_list.html', {'items': items})